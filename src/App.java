public class App {
    int i;
    long j;
    String name;
    public static void main(String[] args) throws Exception {
        try {
            System.out.println("Hello, World!");
            System.out.println(args[0]);
            App a = new App();
            a.i = 10;

            App b = new App();
            b.j=30;
            System.out.println(a.i);
            System.out.println(a.sumNumbersV1());
            System.out.println(b.j);
            System.out.println(b.sumNumbersV1());
            System.out.println("Sum of 1 => 100 is: " + sumNumbersV1());

            //number1 {1, 5, 10}
            int[] number1 = {1, 5, 10};
            System.out.println("Sum of number1 is: " + sumNumbersV2(number1));

            //number2 {1,2,3,5,7,9}
            int[] number2 = new int[]{1,2,3,5,7,9};
            System.out.println("Sum of number2 is: " + sumNumbersV2(number2));

            //in chuỗi
            printHello(24);
            printHello(99);
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }
        
        System.out.println("Continue ...");
    }

    /*Phương thưc tính tổng 100 số tự nhiên */
    public static int sumNumbersV1() {
        int total = 0;
        for (int i=1; i<=100; i++) {
            total += i;
        }
        return total;
    }

    /*Phương thức tính tổng của 1 mảng */
    public static int sumNumbersV2 (int[] arrayNumbers) {
        int total = 0;
        for (int number : arrayNumbers) {
            total += number;
        }
        /* 
        for (int i=0; i<arrayNumbers.length; i++) {
            total += arrayNumbers[i];
        }*/
        return total;
    }

    /*Phương thức in chuỗi */
    public static void printHello(int number) {
        if (number % 2 == 0) {
            System.out.println("Đây là số chẵn");
        } else {
            System.out.println("Đây là số lẻ");
        }
    }
}
